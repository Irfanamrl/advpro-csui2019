package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, double salary){
        if (salary < 30000) {
            throw new IllegalArgumentException();
        }
        else {
            this.name = name;
            this.salary = salary;
        }
    }

    @Override
    public double getSalary() {
        return salary;
    }

    @Override
    public String getRole() {
        return "Front End Programmer";
    }

}
