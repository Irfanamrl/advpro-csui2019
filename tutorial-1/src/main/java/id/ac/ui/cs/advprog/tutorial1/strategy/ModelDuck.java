package id.ac.ui.cs.advprog.tutorial1.strategy;

import java.sql.SQLOutput;

public class ModelDuck extends Duck{
    public ModelDuck() {
        setFlyBehavior(new FlyNoWay());
        setQuackBehavior(new Quack());
    }

    public void display() {
        System.out.println("gue model duck");
    }

    public void swim() {
        System.out.println("cannot swim");
    }
}
