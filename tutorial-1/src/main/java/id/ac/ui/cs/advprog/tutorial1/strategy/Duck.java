package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    //ini method setter
    public void setFlyBehavior(FlyBehavior status) {
      flyBehavior = status;
    }

    public void setQuackBehavior(QuackBehavior status) {
      quackBehavior = status;
    }

    public abstract void display();
    public abstract void swim();
}
