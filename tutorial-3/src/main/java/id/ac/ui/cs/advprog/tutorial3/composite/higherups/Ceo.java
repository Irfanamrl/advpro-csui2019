package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        if (salary < 200000) {
            throw new IllegalArgumentException();
        }
        else {
            this.name = name;
            this.salary = salary;
        }
    }

    @Override
    public double getSalary() {
        return salary;
    }

    @Override
    public String getRole() {
        return "CEO";
    }
}
