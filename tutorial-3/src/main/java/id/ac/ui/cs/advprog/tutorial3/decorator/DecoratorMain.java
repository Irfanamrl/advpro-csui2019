package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;


public class DecoratorMain {

    public static void main(String[] args) {

        Food roti;

        roti = BreadProducer.THICK_BUN.createBreadToBeFilled();

        roti = FillingDecorator.BEEF_MEAT.addFillingToBread(
                roti);

        roti = FillingDecorator.CHEESE.addFillingToBread(
                roti);

        roti = FillingDecorator.CUCUMBER.addFillingToBread(
                roti);

        roti = FillingDecorator.LETTUCE.addFillingToBread(
                roti);

        roti = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                roti);

        System.out.println(roti.cost());

    }
}
